import { createStore } from 'vuex'
import { saveArrayInLocalStorage } from '../helpers/PersistData.js'

export default createStore({
  state: {
    inputValue: '',
    cart: [],
    prices: [],
    totalCart: 0,
    showModal: false
  },
  mutations: {
    getName(state, payload) {
      state.inputValue = payload
    },

    // Function to add in cart
    addCart(state, payload) {
      // Iterable in array to find product
      const exitsProduct = state.cart.find(
        (product) => product.id === payload.id
      )

      // If exist increment quantity, if not add in cart
      if (exitsProduct) {
        exitsProduct.qtdy += 1
        state.prices.push(exitsProduct.price)
      } else {
        state.cart.push(payload)
        saveArrayInLocalStorage(state.cart)
        state.prices.push(payload.price)
      }
    },

    // Function to remove from cart
    decrementFromCart(state, payload) {
      // Iterable in array to find product
      const exitsProduct = state.cart.find(
        (product) => product.id === payload.id
      )

      // If quantity is greater than 1 remove quantitity, otherwise remove product
      // from cart
      if (exitsProduct && exitsProduct.qtdy > 1) {
        exitsProduct.qtdy -= 1
        const indexPrice = state.prices.findIndex(
          (product) => product.price === payload.price
        )
        state.prices.splice(indexPrice, 1)
      } else {
        const index = state.cart.findIndex(
          (product) => product.id === payload.id
        )

        state.cart.splice(index, 1)
        const indexPrice = state.prices.findIndex(
          (product) => product.price === payload.price
        )
        state.prices.splice(indexPrice, 1)
      }
    },

    appearModal(state){
      state.showModal = true;
    },

    // function to clean cart
    cleanCart(state) {
      state.cart = []
    },
  },
  actions: {},
  getters: {
    //return value from input
    filtered(state) {
      return state.inputValue
    },
    // return array in cart
    products(state) {
      return state.cart
    },
    sumTotal(state) {
      let sum =
        state.cart.length === 0
          ? 0
          : state.prices.reduce((previus, current) => previus + current)
      return sum.toFixed(2)
    },
  },
  modules: {},
})
