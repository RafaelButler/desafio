//function save just one obecjt in localstorage
export function saveLocalSotage(product) {
  localStorage.setItem('id', product.id)
  localStorage.setItem('title', product.title)
  localStorage.setItem('brand', product.brand)
  localStorage.setItem('description', product.description)
  localStorage.setItem('price', product.price)
  localStorage.setItem('qtdy', product.qtdy)
}

// function save array in localstorage
export function saveArrayInLocalStorage(products) {
  localStorage.setItem('product', JSON.stringify(products))
}

export function getItemLocalStorage(productId) {
  let products = JSON.parse(localStorage.getItem('product'))

  let productFound = products.find((product) => {
    return product.id === productId
  })

  return productFound
}

export function updateItemLocalStorage(productId, quantity){
  let product = getItemLocalStorage(productId);
  product.qtdy = quantity;
}

export function getAllItemsLocalStorage() {
  let products = localStorage.getItem('product')
  return products
}
