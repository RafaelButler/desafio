<h1 align="center">Desafio</h1>

---

## Technologies

This project was developed with following technologies:

- [Vuejs](https://vuejs.org/)
- [Vue Router](https://router.vuejs.org/)
- [Vuex](https://vuex.vuejs.org/)
- [Sass](https://sass-lang.com/)
  
---  

## How to run

Clone this repo enter in project: 

`cd desafio`

And run command: 

`yarn install`

or

`npm install`

**You can run the command:**

`yarn serve`

or

`npm serve`

Open **localhost:8080** to view it in the browser.

---
